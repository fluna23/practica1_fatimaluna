using System;
//Lo siguiente para los números aleatorios
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Proceso 
{
    public class Carro
    {
        string[] color_carro = new string[5] {"Rojo", "Amarillo", "Verde", "Azul", "Negro"};
        string tapiceria = "Paño", aire = "sin", faros = "normales.";
        int c, costo_carro, a, x;
        // Crear objeto. Utiliza el reloj del sistema para crear una semilla.
        Random rnd = new Random();

        public void exterior(){
            // Obtiene un número natural entre 0 y 5 (incluye el 0)
            c = rnd.Next(5);
            x = rnd.Next(5);

            if (x > 5) {
                faros = "led";
            }

            Console.WriteLine("Tu auto será color " + color_carro[c] + " con faros " + faros);
        }

        public void interior(){
            if (c > 2){
                tapiceria = "Piel";
            }

            a = rnd.Next(9623);

            if (a%2 == 0){
                aire = "con";
            }

            Console.WriteLine("Con tapicería de " + tapiceria + " y " + aire + " aire acondicionado.");
        }

        public void costo(){
            costo_carro = rnd.Next(150000, 200000);
            Console.WriteLine("Y un costo de : $" + costo_carro + " pesos.");
        }
        
    }
}
